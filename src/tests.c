#define _DEFAULT_SOURCE
#include "tests.h"
#include <stdlib.h>
#include <sys/mman.h>

#define HEAP_INIT_SIZE 8192
#define BLOCK_STANDARD_CAPACITY 1024

static inline struct block_header* block_get_header(void* contents) {
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}

bool test1() {
    void* heap = heap_init(HEAP_INIT_SIZE);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* allocate = _malloc(BLOCK_STANDARD_CAPACITY);
    if (allocate == NULL){
        printf("Error: Memory wasn't allocated!\n");
        return false;
    }
    printf("Malloc:\n");
    debug_heap(stdout, heap);
    
    _free(allocate);
    printf("Free\n");
    debug_heap(stdout, heap);

    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;   
}

bool test2() {
    void* heap = heap_init(HEAP_INIT_SIZE);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void *allocate1 = _malloc(BLOCK_STANDARD_CAPACITY);
    void *allocate2 = _malloc(BLOCK_STANDARD_CAPACITY);
    void *allocate3 = _malloc(BLOCK_STANDARD_CAPACITY);
    printf("Mallocs:\n");
    debug_heap(stdout, heap);


    if (allocate1 == NULL || allocate2 == NULL || allocate3 == NULL){
        printf("Error: Memory wasn't allocated!\n");
        return false;
    }    

    _free(allocate1);
    if (allocate2 == NULL || allocate3 == NULL) {
        printf("Release of the first damaged the second and third.\n");
        return false;
    } 
    printf("Releasing:\n");
    debug_heap(stdout, heap);

    _free(allocate2);
    _free(allocate3);
    printf("Free\n");
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;   
}

bool test3() {
    void* heap = heap_init(HEAP_INIT_SIZE);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);
    void *allocate1 = _malloc(BLOCK_STANDARD_CAPACITY);
    void *allocate2 = _malloc(BLOCK_STANDARD_CAPACITY);
    void *allocate3 = _malloc(BLOCK_STANDARD_CAPACITY);
    printf("Mallocs:\n");
    debug_heap(stdout, heap);

    if (allocate1 == NULL || allocate2 == NULL || allocate3 == NULL){
        printf("Error: Memory wasn't allocated!\n");
        return false;
    }    

    _free(allocate1);
    _free(allocate3);
    if (allocate2 == NULL) {
        printf("Release of the first or third damaged the second\n");
        return false;
    }
    _free(allocate2);
    printf("Free\n");
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_INIT_SIZE}).bytes);
    return true;   
}

bool test4() {
    void* heap = heap_init(1);

    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* allocate1 = _malloc(2*HEAP_INIT_SIZE);
    void* allocate2 = _malloc(2*HEAP_INIT_SIZE);
    printf("Mallocs:\n");
    debug_heap(stdout, heap);

    if (allocate1 == NULL || allocate2 == NULL){
        printf("Error: Memory wasn't allocated!\n");
        return false;
    }  

    struct block_header* header1 = block_get_header(allocate1);
    if (!header1 || header1->next != block_get_header(allocate2)) {
        printf("Error: Headers are not linked\n");
        return false;
    }

    _free(allocate1);
    _free(allocate2); 
    printf("After\n");
    debug_heap(stdout, heap);
    
    munmap(heap, header1->capacity.bytes + ((struct block_header*) heap)->capacity.bytes);
    return true;
}

bool test5() {
    void* heap = heap_init(1);
    printf("Initialized heap:\n");
    debug_heap(stdout, heap);

    void* allocate1 = _malloc(8*BLOCK_STANDARD_CAPACITY);
    if (allocate1 == NULL){
        printf("Error: Memory wasn't allocated!\n");
        return false;
    }  
    
    struct block_header* header1 = block_get_header(allocate1);
    if (header1 == NULL) {
        printf("Error: Header not found!\n");
        return false;
    } 
    
    printf("Alloc:\n");
    debug_heap(stdout, heap);

    void* region = mmap( header1->contents + header1->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0 );
    void* allocate2 = _malloc(8*BLOCK_STANDARD_CAPACITY);
    struct block_header* header2 = block_get_header(allocate2);
    if (allocate2 == NULL) {
        printf("Error: Memory wasn't allocated!\n");
        return false;
    }
    printf("Mallocs:");
    debug_heap(stdout, heap);
    
    _free(allocate1);
    _free(allocate2);
    printf("Free:");
    debug_heap(stdout, heap);
    
    munmap(region, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    munmap(region, (header1->capacity.bytes + header2->capacity.bytes + ( (struct block_header*) heap)->capacity.bytes));
   
    return true;
}
