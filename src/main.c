#include "tests.h"
#include <stdio.h>

int main(void){
  size_t tests_passed = 0;
  printf("TEST 1: обычное успешное выделение памяти\n");
  if (test1()) {
    fprintf(stdout, "%s", "TEST 1: PASSED\n");
    tests_passed++;
  }
  else 
    fprintf(stdout, "%s", "TEST 1: FAILED\n");
  printf("__________________________________________________________________\n");

  printf("TEST 2: освобождение одного блока из нескольких выделенных\n");
  if (test2()) {
    fprintf(stdout, "%s", "TEST 2: PASSED\n");
    tests_passed++;
  }
  else
    fprintf(stdout, "%s", "TEST 2: FAILED\n");
  printf("__________________________________________________________________\n");

  printf("TEST 3: Освобождение двух блоков из нескольких выделенных\n");
  if (test3()) {
    fprintf(stdout, "%s", "TEST 3: PASSED\n");
    tests_passed++;
  }
  else
    fprintf(stdout, "%s", "TEST 3: FAILED\n");
  printf("__________________________________________________________________\n");

  printf("TEST 4: Память закончилась, новый регион памяти расширяет старый.\n");
  if (test4()) {
    fprintf(stdout, "%s", "TEST 4: PASSED\n");
    tests_passed++;
  }
  else
    fprintf(stdout, "%s", "TEST 4: FAILED\n");
  printf("__________________________________________________________________\n");

  printf("TEST 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");
  if (test5()) {
    fprintf(stdout, "%s", "TEST 5: PASSED\n");
    tests_passed++;
  }
  else
    fprintf(stdout, "%s", "TEST 5: FAILED\n");
  printf("__________________________________________________________________\n");

  printf("Passed %zu tests out of 5\n", tests_passed);
  
  return 0;
}
